public class FindPrimes {
    public static void main(String[] args) {

        String result = "";
        int x = 1;
        int Number = Integer.parseInt(args[0]);

        if (Number >= 2) {
            while (x < Number) {
                boolean y = true;
                x++;
                for (int i = 2; i < x; i++) {
                    if (x % i == 0) {
                        y = false;
                        break;
                    }
                }
                if (y){
                    result = result + x + ",";
                }
            }
            System.out.print(result);
        }
    }
}
